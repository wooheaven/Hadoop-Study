# Check block location on HDFS
```
ref = http://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html
```
# by fsck
```{bash}
hdfs fsck /user/research/input -files -blocks -locations
```

# real log
```{bash}
$ hdfs fsck /user/research/input -files -blocks -locations
Connecting to namenode via http://BISTel.Research.Dev.NameNode:9870/fsck?ugi=research&files=1&blocks=1&locations=1&path=%2Fuser%2Fresearch%2Finput
FSCK started by research (auth:SIMPLE) from /192.168.3.40 for path /user/research/input at Wed May 30 12:09:49 KST 2018
/user/research/input <dir>
/user/research/input/sample.txt 26 bytes, replicated: replication=2, 1 block(s):  OK
0. BP-165118470-127.0.0.1-1526470454517:blk_1073741830_1006 len=26 Live_repl=2  [DatanodeInfoWithStorage[192.168.3.41:9866,DS-487986d7-2b6b-40a5-a165-c5f5d394916c,DISK], DatanodeInfoWithStorage[192.168.3.42:9866,DS-5bde70ef-cec5-4d35-abe6-4fc7e541cfea,DISK]]


Status: HEALTHY
 Number of data-nodes:    3
 Number of racks:        1
 Total dirs:            1
 Total symlinks:        0

Replicated Blocks:
 Total size:    26 B
 Total files:    1
 Total blocks (validated):    1 (avg. block size 26 B)
 Minimally replicated blocks:    1 (100.0 %)
 Over-replicated blocks:    0 (0.0 %)
 Under-replicated blocks:    0 (0.0 %)
 Mis-replicated blocks:        0 (0.0 %)
 Default replication factor:    2
 Average block replication:    2.0
 Missing blocks:        0
 Corrupt blocks:        0
 Missing replicas:        0 (0.0 %)

Erasure Coded Block Groups:
 Total size:    0 B
 Total files:    0
 Total block groups (validated):    0
 Minimally erasure-coded block groups:    0
 Over-erasure-coded block groups:    0
 Under-erasure-coded block groups:    0
 Unsatisfactory placement block groups:    0
 Average block group size:    0.0
 Missing block groups:        0
 Corrupt block groups:        0
 Missing internal blocks:    0
FSCK ended at Wed May 30 12:09:49 KST 2018 in 7 milliseconds


The filesystem under path '/user/research/input' is HEALTHY
```
